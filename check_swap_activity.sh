#!/bin/sh

# Copyright 2008-2009 Stian Sletner <stian@copyleft.no>.  GPL'd.

# Settings.

DATAFILE=/local/var/nagios_check_swap_activity.vmstat.out
PIDFILE=/local/var/nagios_check_swap_activity.vmstat.pid
VMSTAT_INTERVAL=5	# Seconds.
VMSTAT_MAX=240		# Make vmstat die after this many data points.

# With VMSTAT_INTERVAL=5 and VMSTAT_MAX=240 vmstat will run for no more than
# (240*5)/60 = 20 minutes.  This is just a precaution to make sure it doesn't
# run forever.  Normally the script will kill it off sooner.

# Defaults for command line options.

os=`uname -s | tr A-Z a-z`
warning=20
critical=50

usage () {
	echo "$0 [-w threshold] [-c threshold] [-o {linux|freebsd|...}]"
	exit 3
}

while getopts "c:w:o:t:h" hoi
do
	case $hoi in
		c) critical=$OPTARG ;;
		w) warning=$OPTARG ;;
		o) os=$OPTARG ;;
		*) usage ;;
	esac
done

startvmstat () {
	# Stupid UNIX trick: If you redirect _all three_ of the standard
	# streams when you launch a background process, it won't get killed
	# off after parent's demise.

	vmstat $VMSTAT_INTERVAL $VMSTAT_MAX >$DATAFILE 2>&1 </dev/null &
	echo $! >$PIDFILE || exit 3
}

if [ -r $PIDFILE -a -s $PIDFILE ]
then
	# Make damn sure the vmstat process is dead.

	kill -TERM $(cat $PIDFILE) >/dev/null 2>&1
	sleep 1
	kill -KILL $(cat $PIDFILE) >/dev/null 2>&1
	sleep 1
	rm -f $PIDFILE
else
	startvmstat

	echo "SWAP-ACTIVITY WARNING: PID file is missing/empty/unreadable; assuming this is my first run; nothing to report yet"
	exit 1
fi

# vmstat output differences, one field less on linux
case $os in
	linux)
		field1=7
		field2=8
		;;
	*)
		field1=8
		field2=9
		;;
esac

judge () {
	if [ $1 -gt $critical -o $2 -gt $critical ]
	then
		xs=2; judgement=CRITICAL
	elif [ $1 -gt $warning -o $2 -gt $warning ]
	then
		xs=1; judgement=WARNING
	else
		xs=0; judgement=OK
	fi

	echo "SWAP-ACTIVITY $judgement: Average pages in=$1 out=$2, max seen in=$4 out=$5 last $(( $VMSTAT_INTERVAL * $3 / 60 )) minutes ($3 data points)"
}

sums=$(awk 'BEGIN {
		pi = 0
		po = 0
		count = 0
		mi = 0
		mo = 0
	}
	/^ *[0-9]/ {
		i = $'$field1'
		o = $'$field2'
		pi += i
		po += o
		if (i > mi) mi = i
		if (o > mo) mo = o
		count++
	}
	END {
		print int(pi/count), int(po/count), count, mi, mo
	}' $DATAFILE)

judge $sums

# Start it up again.
startvmstat

exit $xs
